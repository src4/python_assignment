# numbers and math

print(3+4) #7
print ("3+4") #string concatenation through +
print (3/4) #0.75
print (round(3/4)) #1
print(3%4) #3
print(5*2) #10
print(5>2) #True
print(2>5) #False
print(2!=1)#True
print(2<5&5>3) #True
print(2<5&5>7) #False
print(2>5&5<7) #False
